global.__base           = __dirname + '/';

var Path            = require('path');
    global.Config   = require( Path.resolve( __dirname, 'config/dev.js' ) );

var Hapi            = require('hapi');

var server  = new Hapi.Server(),
    Velib   = require(__base + 'app/lib/Velib/api.js')('387786ff0ccd4b1e7eec4c79da78b3c5d05f83a9');

server.connection({
    host: Config.app.host,
    port: Config.app.port
});


server.route({
    method: 'GET',
    path:'/api/cities',
    handler: Velib.getCities
});

server.route({
    method: 'GET',
    path:'/api/stations/{city}',
    handler: Velib.getStations
});

server.route({
    method: 'GET',
    path:'/api/station/{city}/{station}',
    handler: Velib.getStation
});

server.route({
    method: 'GET',
    path:'/api/stations/{city}/nearby/{lat}/{lng}/{radius}',
    handler: Velib.getStationsNearby
});

server.start(function() {
     console.log('Server running at:', server.info.uri);
});