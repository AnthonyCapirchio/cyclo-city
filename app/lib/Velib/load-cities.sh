#!/bin/sh   

Cities=( "Bruxelles-Capitale" "Namur" "Santander" "Seville" "Valence" "Amiens" "Besancon" "Cergy-Pontoise" "Creteil" "Lyon" "Marseille" "Mulhouse" "Nancy" "Nantes" "Paris" "Rouen" "Toulouse" "Dublin" "Toyama" "Vilnius" "Luxembourg" "Lillestrom" "Kazan" "Goteborg" "Lund" "Stockholm" "Ljubljana" )

base_url="https://developer.jcdecaux.com/rest/vls/stations/"

for element in "${Cities[@]}"    
do
	wget -P $(dirname $0)"/data/cities" $base_url$element".json"
done