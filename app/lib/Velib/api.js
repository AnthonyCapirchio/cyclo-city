const RADIUS_MILES  = 3959; // R - Miles
const RADIUS_KM     = 6371; // R - Km


var Path 		= require('path');
var Q 			= require('q');
var request 	= require('request');
var stations;
var cities 		= ['Paris'];



/**
 * [toString description]
 * @param  {mixed}  value [description]
 * @return {string}       [description]
 */
function toString(value){
    switch(typeof value){
        case 'number':
            return "" + value;
    }
}

/**
 * Degrees to radians conversion
 *
 * rad = degrees ⋅ π/180
 *
 * @param  {float} degrees  value in degrees to convert
 * @return {float}          converted dregrees to radian value
 */
function deg2rad(degrees) {
    return degrees * Math.PI/180;
}

/**
 * The round() function returns the value of a number rounded to the nearest /1000.
 *
 *      (A)round the world, (a)round the world
 *      (A)round the world, (a)round the world
 *      (A)round the world, (a)round the world
 *      (A)round the world, (a)round the world
 *      ...
 *
 * @param  {float} x
 * @return {float}
 */
function round(x) {
    return Math.round( x * 1000) / 1000;
}


/**
 * Get distance between two coordinates with the Haversine formula.
 * This function return the distance in Km. If it's specified by the `metric` parameter,
 * the function return the distance in miles.
 *
 *      a = sin²(Δφ/2) + cos φ1 ⋅ cos φ2 ⋅ sin²(Δλ/2)
 *      c = 2 ⋅ atan2( √a, √(1−a) )
 *      d = R ⋅ c
 *
 *      φ is latitude, λ is longitude, R is earth’s radius (mean radius = 6,371km);
 *      note that angles need to be in radians to pass to trig functions!
 *
 * @param   {object}  cfg       object contains coordinates values
 *
 *       {
 *          from: {
 *              lat: {float} latitude in decimal degrees
 *              lng: {float} longitude in decimal degrees
 *          },
 *          to:{
 *              lat: {float} latitude in decimal degrees
 *              lng: {float} longitude in decimal degrees
 *          }
 *       }
 *
 * @param   {string}  metric    metric for the distance. 'km', 'miles', 'km' by default
 * @return  {float}             distance between the coordinates
 */
function Distance(cfg, metric) {

    var lat1, lon1, lat2, lon2, dlat, dlon, a, c, dm, dk, mi, km;

    // convert coordinates to radians
    lat1 = deg2rad( cfg.from.lat );
    lon1 = deg2rad( cfg.from.lng );
    lat2 = deg2rad( cfg.to.lat );
    lon2 = deg2rad( cfg.to.lng );

    // find the differences between the coordinates
    dlat = lat2 - lat1;
    dlon = lon2 - lon1;

    a  = Math.pow(Math.sin(dlat/2),2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(dlon/2),2);
    c  = 2 * Math.atan2(Math.sqrt(a),Math.sqrt(1-a)); // great circle distance in radians

    dm = c * RADIUS_MILES;  // great circle distance in miles
    dk = c * RADIUS_KM;     // great circle distance in km

    // round the results down to the nearest 1/1000
    mi = round(dm);
    km = round(dk);

    return metric === 'miles' ? miles : km;
}


function searchStationNearby(city, lat, lng, radius){

	loadStations(city);

	var stationsNearby = stations.filter(function(entry){

		var config = {
			from: {
				lat: entry.latitude,
				lng: entry.longitude
			},
			to: {
				lat: lat,
				lng: lng
			}
		};

		return Distance(config) < radius;
	});

	stationsNearby.sort(function(a, b){
		var distanceA = Distance({
		  		from: {
					lat: a.latitude,
					lng: a.longitude
				},
				to: {
					lat: lat,
					lng: lng
				}
			});

		  var distanceB = Distance({
		  		from: {
					lat: b.latitude,
					lng: b.longitude
				},
				to: {
					lat: lat,
					lng: lng
				}
			});


		  if (distanceA < distanceB) return -1;
		  if (distanceB > distanceA) return 1;

		  return 0;
	});

	return stationsNearby;
}



function loadStations(city){

	var cityPath = Path.resolve(__dirname, 'data/static/', city + '.json');

	stations = require( cityPath );
}


function loadAvailabilities(city, stationId){
	var deferred 	= Q.defer();
	var target  	= 'https://api.jcdecaux.com/vls/v1/stations/' + stationId + '?contract=' + city + '&apiKey=387786ff0ccd4b1e7eec4c79da78b3c5d05f83a9';

	request(target, function(error, response, body){
		deferred.resolve( JSON.parse(body) );
	});


	return deferred.promise;
}


function searchStation(city, stationId){

	loadStations(city);

	var station = stations.filter(function(value){
			return value.number == stationId;
		});


	return station[0];
}

module.exports = function(apiKey){


	if(!apiKey){
		return reply();
	}


	return {

		getCities: function(request, reply){
			reply(cities);
		},

		getStations: function(request, reply){
			// request.params.city

			loadStations( request.params.city );

			reply(stations);
		},


		getStation: function(request, reply){

			var city 		= request.params.city,
				stationId 	= request.params.station,
				full 		= request.query.full || false,
				station 	= searchStation(city, stationId);

			if(!station) 	return reply().code(204);
			if(!full) 		return reply(station);

			loadAvailabilities(city, stationId)
				.then(
					function(result){
						reply(result);
					},
					function(){
						reply().code(204);
					});

		},

		getStationsNearby: function(request, reply){
			var city 		= request.params.city,
				latitude 	= request.params.lat,
				longitude 	= request.params.lng,
				radius 		= request.params.radius,
				full 		= request.query.full || false,
				stations 	= searchStationNearby(city, latitude, longitude, radius);

			reply(stations);
		}



	}
};